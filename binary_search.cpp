// Proyecto 6 - Traduccion con busqueda binaria

#include <iostream>
#include <chrono>
#include <fstream>


using namespace std::chrono;
using namespace std;

// Todos los codones y sus aminoacidos correspondientes
string codons[] = {"AAA", "AAC", "AAG", "AAU", "ACA", "ACC", "ACG", "ACU",
                   "AGA", "AGC", "AGG", "AGU", "AUA", "AUC", "AUG", "AUU",
                   "CAA", "CAC", "CAG", "CAU", "CCA", "CCC", "CCG", "CCU",
                   "CGA", "CGC", "CGG", "CGU", "CUA", "CUC", "CUG", "CUU",
                   "GAA", "GAC", "GAG", "GAU", "GCA", "GCC", "GCG", "GCU",
                   "GGA", "GGC", "GGG", "GGU", "GUA", "GUC", "GUG", "GUU",
                   "UAA", "UAC", "UAG", "UAU", "UCA", "UCC", "UCG", "UCU",
                   "UGA", "UGC", "UGG", "UGU", "UUA", "UUC", "UUG", "UUU"};
string aminos[] = {"K", "N", "K", "N", "T", "T", "T", "T",
                   "R", "S", "R", "S", "I", "I", "M", "I",
                   "Q", "H", "Q", "H", "P", "P", "P", "P",
                   "R", "R", "R", "R", "L", "L", "L", "L",
                   "E", "D", "E", "D", "A", "A", "A", "A",
                   "G", "G", "G", "G", "V", "V", "V", "V",
                   "!", "Y", "!", "Y", "S", "S", "S", "S",
                   "!", "C", "W", "C", "L", "F", "L", "F"};

int length = sizeof(codons)/sizeof(codons[0]);

// Suma de todos los elementos de un arreglo
int sum(int array[], int size){
    int sum = 0;
    for (int i=0; i < size; i++){
        sum += array[i];
    }
    return sum;
}

// Busqueda binaria iterativa
int binary_search(string codon){
    int left = 0;
    int right = 63;

    while (left <= right) {
        int mid = ((left+right)/2);

        if (codons[mid].compare(codon) < 0) {
            right = mid - 1;
        } else if (codons[mid].compare(codon) > 0) {
            left = mid + 1;
        } else {
            return mid;
        }
    }
    return -1;
}

// Traducion de una sequencia a su forma aminoacidica
string traduction(string sequence){
    string codon = "";
    int floor = 0;
    string aminoacid = "";
    for (int i=0; i < sequence.length()+1; i+=3){
        for (int j=floor; j<i; j++){
            codon += sequence[j];
            floor = j+1;
        }
        aminoacid += aminos[binary_search(codon)];
        codon = "";
    }
    return aminoacid;
}

int main(){

    string sequence;
    string translated;
    int times[42];
    int i=0;

    // Se ejecuta la traduccion de 42 secuencias, midiendo el tiempo
    ifstream seq_file("cadenas_arn.txt");
    while (getline(seq_file, sequence)){

        auto start = high_resolution_clock::now();
        translated = traduction(sequence);
        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<microseconds>(stop - start);

        times[i] = duration.count();
        cout << "Secuencia " << sequence << " demora " << times[i] << "ms" << endl;
        i++;
    }
    seq_file.close();

    // Se muestra el tiempo en traducir y su promedio
    cout << "Traduccion de 42 secuencias toma " << sum(times, 42) << "ms" << endl;
    cout << "Promedio de " << sum(times, 42)/42.0 << "ms" << endl;

    return 0;
}
