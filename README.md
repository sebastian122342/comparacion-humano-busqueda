# Proyecto 2: Comparación de tiempos de traducción humano - algoritmos de búsqueda

En el repositorio se encontrarán dos archivos: uno que ejecuta traducciones mediante
algoritmos de búsqueda lineal mientras que el otro lleva a cabo búsqueda binaria.

## Integrantes
* Sebastián Bustamante
* Magdalena Lolas
* Diego Núñez
